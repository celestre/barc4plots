#!/usr/bin/python
# coding: utf-8
###################################################################################
# BARC ('Biblioteca Auxiliar do Rafael Celestre') for Plotting & Image manipulation
# Authors/Contributors: Rafael Celestre
# Rafael.Celestre@esrf.fr
# creation: 25.07.2018
# previously updated: 18.06.2019
# last update: 21.04.2021 (v.03)
###################################################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import ticker, rcParamsDefault
from matplotlib.widgets import RectangleSelector, EllipseSelector
from matplotlib.colors import LogNorm, PowerNorm
from scipy.interpolate import interp2d, RectBivariateSpline
from copy import deepcopy


# ****************************************************************************
# ********************** Class for images handling
# ****************************************************************************


class Image2Plot:
    """
    Class for handling 2D numpy arrays and plotting.
    """

    def __init__(self, image=None, axis_x=None, axis_y=None):

        # basic elements of the class
        self.image = image  # 1D or 2D numpy array
        self.x = axis_x  # horizontal axis
        self.y = axis_y  # vertical axis
        # additional info for plotting
        self.ax_limits = [None, None, None, None]  # [xmin, xmax, ymin, ymax] - overwrites self.ranges for 1D plot
        self.plt_limits = [None, None]  # [min, max] values on a 2D graph
        self.legends = ['Graph name', 'x-axis', 'y-axis']
        self.xticks = None
        self.xticksaxis = None
        self.twinAxis = False
        # Aesthetics
        self.AspectRatio = True
        self.ColorScheme = 0  # see ESRF_colours() and ESRF_colours_2D()
        self.Colorbar = True
        self.ColorbarExt = 'neither'  # [ 'neither' | 'both' | 'min' | 'max' ]
        self.Scale = 0  # 2D plot: 0 - linear; 1 - log10; 2: Gamma = 0.25.
        # 1D plot: 0 - linear; 1 - semilogy; 2 - semilogx; 3 - loglog
        self.Gamma = 0.25
        self.LaTex = False  # Boolean - LaTex fonts when compiling
        self.FontsSize = 'presentation'  # presentation (small fonts), paper (large fonts)
        self.FontsSizeScale = 1  # scaling factor for the FigSize fonts
        self.grid = False
        self.nbins = 4  # number of max of bins (Maximum number of intervals; one less than max number of ticks)
        # 1D plot-specific arguments
        self.label = None  # for 1D plot, a label to the curve
        self.LabelPos = 0  # 'best' 0; 'upper right' 1; 'upper left' 2; 'lower left' 3; 'lower right' 4;
        # 'right' 5; 'center left' 6; 'center right' 7; 'lower center' 8; 'upper center' 9;
        # 'center' 10
        self.LineStyle = '-'  # '-', '--', '-.', ':'
        # "o" circle; "v" triangle_down; "^" triangle_up; ">" triangle_right; "<" triangle_left;  "8" octagon;
        # "s" square; "p" pentagon; "P" plus (filled); "*" star; "h" hexagon1; "H" hexagon2; "+" plus; "x" x;
        # "X" x (filled); "D" diamond;  "d" thin_diamond
        self.FillBetween = False  # for 1D plot, fill in between the curve and FillBetweenValue with alpha 50%
        self.FillBetweenValue = 0
        self.alpha = 1
        # 3D plot-specific arguments
        self.Style3D = 'surf'  # contour, wire, surf

    def sort_class(self):
        # Axes, limits and ranges
        if self.x is None:
            if self.image.ndim == 2:
                self.x = np.linspace(-self.image.shape[1] / 2, self.image.shape[1] / 2, self.image.shape[1])
            else:  # 1D array
                self.x = np.linspace(-self.image.shape[0] / 2, self.image.shape[0] / 2, self.image.shape[0])
        if self.y is None:
            if self.image.ndim == 2:
                self.y = np.linspace(-self.image.shape[0] / 2, self.image.shape[0] / 2, self.image.shape[0])
        if self.ax_limits[0] is None:
            self.ax_limits[0] = self.x[0]
        if self.ax_limits[1] is None:
            self.ax_limits[1] = self.x[-1]
        try:
            if self.image.ndim == 2:
                if self.ax_limits[2] is None:
                    self.ax_limits[2] = self.y[0]
                if self.ax_limits[3] is None:
                    self.ax_limits[3] = self.y[-1]
        except:  # patch for quiver plot
            if self.ax_limits[2] is None:
                self.ax_limits[2] = self.y[0]
            if self.ax_limits[3] is None:
                self.ax_limits[3] = self.y[-1]

    def get_ROI_coords(self, coords='p', roi='r'):
        """

        :param coords: 'p' for pixel, 'a' for using x- and y-axis,
        :param roi: 'r' rectangular and 'c' for circular,
        :return:
        """
        self.Colorbar = False
        self.sort_class()
        (Xi, Xf, Yi, Yf) = plot_2D(self, Crop=True, ROI=roi)

        if coords == 'p':
            Xi = int(np.argmin(np.abs(self.x - Xi)))
            Xf = int(np.argmin(np.abs(self.x - Xf)))
            Yi = int(np.argmin(np.abs(self.y - Yi)))
            Yf = int(np.argmin(np.abs(self.y - Yf)))

        return Xi, Xf, Yi, Yf

    def crop_image(self):
        pass

    def mask_image(self):
        pass

    def scale_image(self):
        print('Place holder: not implemented yet...')


# ****************************************************************************
# ********************** Colors schemes
# ****************************************************************************

def ESRF_colors(_scheme):
    """
    ESRF colour pallet for 1D plots
    :param _scheme: color number
    :return: rgb for the chosen colour
    """
    if type(_scheme) == np.ndarray:
        return _scheme
    elif type(_scheme) == int:
        if _scheme == -2:  # red
            rgb = (255. / 255, 000. / 255, 000. / 255)
        if _scheme == -1:  # black
            rgb = (000. / 255, 000. / 255, 000. / 255)
        elif _scheme == 0:  # dark blue
            rgb = (019. / 255, 037. / 255, 119. / 255)
        elif _scheme == 1:  # green
            rgb = (081. / 255, 160. / 255, 038. / 255)
        elif _scheme == 2:  # magenta
            rgb = (175. / 255, 000. / 255, 124. / 255)
        elif _scheme == 3:  # dark oranges
            rgb = (237. / 255, 119. / 255, 003. / 255)
        elif _scheme == 4:  # cyan
            rgb = (000. / 255, 152. / 255, 212. / 255)
        # new colors - pastel colours
        elif _scheme == 5:  # yellow
            rgb = (232. / 255, 186. / 255, 134. / 255)
        elif _scheme == 6:  # dark green
            rgb = (001. / 255, 105. / 255, 118. / 255)
        elif _scheme == 7:  # pink
            rgb = (222. / 255, 033. / 255, 115. / 255)
        elif _scheme == 8:  # dark blue
            rgb = (002. / 255, 085. / 255, 127. / 255)
        elif _scheme == 9:  # light green
            rgb = (207. / 255, 208. / 255, 140. / 255)
        elif _scheme == 10:  # pale blue
            rgb = (078. / 255, 091. / 255, 153. / 255)
        # Qualitative colormap
        elif _scheme == 101:
            rgb = (031. / 255, 119. / 255, 180. / 255)
        elif _scheme == 102:
            rgb = (174. / 255, 199. / 255, 232. / 255)
        elif _scheme == 103:
            rgb = (255. / 255, 127. / 255, 014. / 255)
        elif _scheme == 104:
            rgb = (255. / 255, 187. / 255, 120. / 255)
        elif _scheme == 105:
            rgb = (044. / 255, 160. / 255, 044. / 255)
        elif _scheme == 106:
            rgb = (152. / 255, 223. / 255, 138. / 255)
        elif _scheme == 107:
            rgb = (214. / 255, 039. / 255, 040. / 255)
        elif _scheme == 108:
            rgb = (255. / 255, 152. / 255, 150. / 255)
        elif _scheme == 109:
            rgb = (148. / 255, 103. / 255, 189. / 255)
        elif _scheme == 110:
            rgb = (197. / 255, 176. / 255, 213. / 255)
        elif _scheme == 111:
            rgb = (140. / 255, 086. / 255, 075. / 255)
        elif _scheme == 112:
            rgb = (196. / 255, 156. / 255, 148. / 255)
        elif _scheme == 113:
            rgb = (227. / 255, 119. / 255, 194. / 255)
        elif _scheme == 114:
            rgb = (247. / 255, 182. / 255, 210. / 255)
        elif _scheme == 115:
            rgb = (127. / 255, 127. / 255, 127. / 255)
        elif _scheme == 116:
            rgb = (199. / 255, 199. / 255, 199. / 255)
        elif _scheme == 117:
            rgb = (188. / 255, 189. / 255, 034. / 255)
        elif _scheme == 118:
            rgb = (219. / 255, 219. / 255, 114. / 255)
        elif _scheme == 119:
            rgb = (023. / 255, 190. / 255, 207. / 255)
        elif _scheme == 120:
            rgb = (158. / 255, 218. / 255, 229. / 255)
        elif _scheme == 200:
            rgb = (255. / 255, 166. / 255, 0. / 255)
        elif _scheme == 201:
            rgb = (255. / 255, 0. / 255, 0. / 255)
        elif _scheme == 202:
            rgb = (0. / 255, 255. / 255, 0. / 255)
        elif _scheme == 203:
            rgb = (0. / 255, 99. / 255, 0. / 255)
        elif _scheme == 204:
            rgb = (0. / 255, 255. / 255, 255. / 255)
        elif _scheme == 205:
            rgb = (0. / 255, 0. / 255, 255. / 255)
        return rgb

    else:
        print('Colour palette does not exist')


def ESRF_colors_2D(_scheme):
    """
    Color maps for 2D plots
    :param _scheme:
    :return:
    """
    # Colour maps reference: https://matplotlib.org/examples/color/colormaps_reference.html
    if _scheme == 0:  # white(0)/yellow/orange/blue/black(1)
        colormap = cm.CMRmap_r
    elif _scheme == 1:  # white(1)/yellow/orange/blue/black(0)
        colormap = cm.CMRmap
    elif _scheme == 2:  # white(1)/black(0) - mimics SRW
        colormap = cm.binary_r
    elif _scheme == 3:  # red(1)/white/black(0) diverging colormap
        colormap = cm.RdGy_r
    elif _scheme == 4:  # Segmented colour map
        colormap = cm.tab20c
    elif _scheme == 5:  # red(1)-yellow-green-blue(0) - Jet colormap
        colormap = cm.jet
    elif _scheme == 6:  # Viridis (perceptually uniform colormaps - inverted)
        colormap = cm.viridis_r
    elif _scheme == 7:  # Viridis
        colormap = cm.viridis
    elif _scheme == 8:  # phase - PyNx
        colormap = cm.hsv
    elif _scheme == 9:  # Inverted Cube helix
        colormap = cm.cubehelix_r
    elif _scheme == 10:  # Cube helix
        colormap = cm.cubehelix
    elif _scheme == 11:  # Diverging colormap blue to red
        colormap = cm.bwr
    elif _scheme == 12:  # Diverging colormap France
        colormap = cm.RdBu
    elif _scheme == 13:  # inverted SRW colormap
        colormap = cm.binary
    else:  # give by hand the desired colormap
        colormap = _scheme
    return colormap


# ****************************************************************************
# ********************** other settings
# ****************************************************************************


def plt_settings(_figsize=0, _k=1, _latexstyle=True, _hold=False, _silent=False, m=6.4, n=4.8):
    """

    :param _figsize:
    :param _k:
    :param _latexstyle:
    :param _hold:
    :param m:
    :param n:
    :return:
    """
    plt.rcParams.update(rcParamsDefault)
    if _latexstyle:
        plt.rcParams.update({
            "text.usetex": False,
            # "font.family": "serif",
            # "font.serif": ["Palatino"]
            "font.family": "DeJavu Serif",
            "font.serif": ["Times New Roman"]
            # "font.family": "serif",
            # "font.serif": ["Computer Modern Roman"]
        })

    if _hold is False:
        if _silent is True:
            plt.close()
        fig = plt.figure(figsize=(m, n))

    plt.gcf().subplots_adjust(left=0.15)

    # plt.rc('font', size=18*_k)  # controls default text sizes
    plt.rc('axes', titlesize=16 * _k)  # fontsize of the axes title
    plt.rc('axes', labelsize=15 * _k)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=14 * _k)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=14 * _k)  # fontsize of the tick labels
    plt.rc('legend', fontsize=13 * _k)  # legend fontsize
    # plt.rc('figure', titlesize=30*_k)  # fontsize of the figure title


# ****************************************************************************
# ********************** 1D plots
# ****************************************************************************


def plot_1D(plt_cntnr, file_name=None, Hold=False, Enable=True, Silent=False, m=6.4 * 1.2, n=4.8, dpi=500):
    """

    :param plt_cntnr:
    :param file_name:
    :param Hold:
    :param Enable:
    :param Silent:
    :param m:
    :param n:
    :param dpi:
    :return:
    """
    plt_settings(plt_cntnr.FontsSize, plt_cntnr.FontsSizeScale, plt_cntnr.LaTex, _hold=Hold, m=m, n=n)

    if plt_cntnr.legends[0] is not None:
        plt.title(plt_cntnr.legends[0])
    if plt_cntnr.legends[1] is not None:
        plt.xlabel(plt_cntnr.legends[1])
    if plt_cntnr.legends[2] is not None:
        plt.ylabel(plt_cntnr.legends[2])

    plt.tick_params(direction='in', which='both')
    plt.tick_params(axis='x', pad=8)

    if plt_cntnr.ax_limits[1] is None:
        if plt_cntnr.ax_limits[0] is None:
            pass
        else:
            plt.xlim(xmin=plt_cntnr.ax_limits[0])
    else:
        if plt_cntnr.ax_limits[0] is None:
            plt.xlim(xmax=plt_cntnr.ax_limits[1])
        else:
            plt.xlim((plt_cntnr.ax_limits[0], plt_cntnr.ax_limits[1]))

    if plt_cntnr.ax_limits[3] is None:
        if plt_cntnr.ax_limits[2] is None:
            pass
        else:
            plt.ylim(ymin=plt_cntnr.ax_limits[2])
    else:
        if plt_cntnr.ax_limits[2] is None:
            plt.ylim(ymax=plt_cntnr.ax_limits[3])
        else:
            plt.ylim((plt_cntnr.ax_limits[2], plt_cntnr.ax_limits[3]))

    if plt_cntnr.label is not None:
        if plt_cntnr.Scale == 0:
            im = plt.plot(plt_cntnr.x, plt_cntnr.image, plt_cntnr.LineStyle, label=plt_cntnr.label)
            plt.setp(im, color=ESRF_colors(plt_cntnr.ColorScheme), alpha=plt_cntnr.alpha)
        elif plt_cntnr.Scale == 1:
            im = plt.semilogy(plt_cntnr.x, plt_cntnr.image, plt_cntnr.LineStyle, label=plt_cntnr.label)
            plt.setp(im, color=ESRF_colors(plt_cntnr.ColorScheme), alpha=plt_cntnr.alpha)
        elif plt_cntnr.Scale == 2:
            im = plt.semilogx(plt_cntnr.x, plt_cntnr.image, plt_cntnr.LineStyle, label=plt_cntnr.label)
            plt.setp(im, color=ESRF_colors(plt_cntnr.ColorScheme), alpha=plt_cntnr.alpha)
        elif plt_cntnr.Scale == 3:
            im = plt.loglog(plt_cntnr.x, plt_cntnr.image, plt_cntnr.LineStyle, label=plt_cntnr.label)
            plt.setp(im, color=ESRF_colors(plt_cntnr.ColorScheme), alpha=plt_cntnr.alpha)
        plt.legend(loc=plt_cntnr.LabelPos)

    else:
        if plt_cntnr.Scale == 0:
            im = plt.plot(plt_cntnr.x, plt_cntnr.image, plt_cntnr.LineStyle)
            plt.setp(im, color=ESRF_colors(plt_cntnr.ColorScheme), alpha=plt_cntnr.alpha)
        elif plt_cntnr.Scale == 1:
            im = plt.semilogy(plt_cntnr.x, plt_cntnr.image, plt_cntnr.LineStyle)
            plt.setp(im, color=ESRF_colors(plt_cntnr.ColorScheme), alpha=plt_cntnr.alpha)
        elif plt_cntnr.Scale == 2:
            im = plt.semilogx(plt_cntnr.x, plt_cntnr.image, plt_cntnr.LineStyle)
            plt.setp(im, color=ESRF_colors(plt_cntnr.ColorScheme), alpha=plt_cntnr.alpha)
        elif plt_cntnr.Scale == 3:
            im = plt.loglog(plt_cntnr.x, plt_cntnr.image, plt_cntnr.LineStyle)
            plt.setp(im, color=ESRF_colors(plt_cntnr.ColorScheme), alpha=plt_cntnr.alpha)

    if plt_cntnr.grid:
        plt.grid(which='major', linestyle='--', linewidth=0.5, color='dimgrey')
        plt.grid(which='minor', linestyle='--', linewidth=0.5, color='lightgrey')

    if plt_cntnr.FillBetween:
        plt.fill_between(plt_cntnr.x, plt_cntnr.FillBetweenValue, plt_cntnr.image,
                         color=ESRF_colors(plt_cntnr.ColorScheme), alpha=plt_cntnr.alpha)

    if plt_cntnr.Scale != 0 or Hold is False:
        plt.locator_params(tight=True)  # , nbins=plt_cntnr.nbins)

    if plt_cntnr.xticks is not None:
        plt.xticks(plt_cntnr.xticksaxis, plt_cntnr.xticks, rotation=0, horizontalalignment='center')

    if file_name is not None:
        plt.savefig(file_name, dpi=dpi, bbox_inches='tight')
        file_name = file_name.split('/')
        print('>>>> file %s saved to disk.' % file_name[-1])
        if Silent:
            plt.close()

    if Enable:
        plt.show()


# ****************************************************************************
# ********************** 2D plots
# ****************************************************************************


def plot_2D(plt_cntnr, file_name=None, Crop=False, ROI='r', Scale=False, Enable=True, Silent=False, isphase=False,
            m=6.4, n=4.8, dpi=500):
    """

    :param plt_cntnr:
    :param file_name:
    :param Crop:
    :param ROI:
    :param Scale:
    :param Enable:
    :param Silent:
    :param isphase:
    :param m:
    :param n:
    :param dpi:
    :return:
    """

    plt_settings(plt_cntnr.FontsSize, plt_cntnr.FontsSizeScale, plt_cntnr.LaTex, _hold=False, m=m, n=n)

    if plt_cntnr.legends[0] is not None:
        plt.title(plt_cntnr.legends[0])
    if plt_cntnr.legends[1] is not None:
        plt.xlabel(plt_cntnr.legends[1])
    if plt_cntnr.legends[2] is not None:
        plt.ylabel(plt_cntnr.legends[2])

    plt.tick_params(direction='in', which='both')
    plt.tick_params(axis='x', pad=8)

    if plt_cntnr.ax_limits[1] is None:
        if plt_cntnr.ax_limits[0] is None:
            pass
        else:
            plt.xlim(xmin=plt_cntnr.ax_limits[0])
    else:
        if plt_cntnr.ax_limits[0] is None:
            plt.xlim(xmax=plt_cntnr.ax_limits[1])
        else:
            plt.xlim((plt_cntnr.ax_limits[0], plt_cntnr.ax_limits[1]))

    if plt_cntnr.ax_limits[3] is None:
        if plt_cntnr.ax_limits[2] is None:
            pass
        else:
            plt.ylim(ymin=plt_cntnr.ax_limits[2])
    else:
        if plt_cntnr.ax_limits[2] is None:
            plt.ylim(ymax=plt_cntnr.ax_limits[3])
        else:
            plt.ylim((plt_cntnr.ax_limits[2], plt_cntnr.ax_limits[3]))

    if plt_cntnr.Scale == 0:
        im = plt.imshow(plt_cntnr.image, cmap=ESRF_colors_2D(plt_cntnr.ColorScheme),
                        extent=[plt_cntnr.x[0], plt_cntnr.x[-1], plt_cntnr.y[0], plt_cntnr.y[-1]], origin='lower',
                        vmax=plt_cntnr.plt_limits[1], vmin=plt_cntnr.plt_limits[0])
    elif plt_cntnr.Scale == 1:
        im = plt.imshow(plt_cntnr.image, cmap=ESRF_colors_2D(plt_cntnr.ColorScheme),
                        extent=[plt_cntnr.x[0], plt_cntnr.x[-1], plt_cntnr.y[0], plt_cntnr.y[-1]], origin='lower',
                        norm=LogNorm(vmax=plt_cntnr.plt_limits[1], vmin=plt_cntnr.plt_limits[0]))
    else:
        im = plt.imshow(plt_cntnr.image, cmap=ESRF_colors_2D(plt_cntnr.ColorScheme),
                        extent=[plt_cntnr.x[0], plt_cntnr.x[-1], plt_cntnr.y[0], plt_cntnr.y[-1]], origin='lower',
                        norm=PowerNorm(plt_cntnr.Gamma, vmax=plt_cntnr.plt_limits[1], vmin=plt_cntnr.plt_limits[0]))

    def format_func(x, pos):
        x = '%.2f' % x
        pad = '' if x.startswith('-') else ' '
        return '{}{}'.format(pad, x)

    if plt_cntnr.Colorbar:
        if plt_cntnr.Scale == 1:
            if plt_cntnr.AspectRatio:
                im_ratio = (plt_cntnr.y[-1] - plt_cntnr.y[0]) / (plt_cntnr.x[-1] - plt_cntnr.x[0])
            else:
                im_ratio = 1
            cb = plt.colorbar(im, fraction=0.046 * im_ratio, pad=0.04, extend=plt_cntnr.ColorbarExt, format='%.0e')

        else:

            if plt_cntnr.AspectRatio:
                im_ratio = (plt_cntnr.y[-1] - plt_cntnr.y[0]) / (plt_cntnr.x[-1] - plt_cntnr.x[0])
            else:
                im_ratio = 1
            if isphase:
                cb = plt.colorbar(im, fraction=0.046 * im_ratio, pad=0.04, extend=plt_cntnr.ColorbarExt,
                                  spacing='uniform', ticks=[-np.pi, 0, np.pi])
                cb.ax.set_yticklabels(['$-\pi$', '0', '$\pi$'])
            else:
                cb = plt.colorbar(im, fraction=0.046 * im_ratio, pad=0.04, extend=plt_cntnr.ColorbarExt,
                                  spacing='uniform')

                # if plt_cntnr.Scale != 2:
                #     def format_func(x, pos):
                #         x = '%.2f' % x
                #         pad = '' if x.startswith('-') else ' '
                #         return '{}{}'.format(pad, x)

                tick_locator = ticker.MaxNLocator(nbins=4)
                cb.locator = tick_locator
                cb.update_ticks()
                if plt_cntnr.Scale != 2:
                    cb.ax.yaxis.major.formatter = ticker.FuncFormatter(format_func)

    plt.locator_params(tight=True, nbins=plt_cntnr.nbins)

    if plt_cntnr.grid:
        plt.grid(linestyle='--', linewidth=0.4, color='dimgrey')

    if plt_cntnr.AspectRatio:
        im._axes._axes.set_aspect('equal')
    else:
        im._axes._axes.set_aspect('auto')

    if file_name is not None:
        plt.savefig(file_name, dpi=dpi, bbox_inches='tight')
        file_name = file_name.split('/')
        print('>>>> file %s saved to disk.' % file_name[-1])
        if Silent:
            plt.close()

    if Crop is True or Scale is True:

        current_ax = im._axes  # make a new plotting ranges

        def line_select_callback(eclick, erelease):
            'eclick and erelease are the press and release events'
            x1, y1 = eclick.xdata, eclick.ydata
            x2, y2 = erelease.xdata, erelease.ydata
            print("(X1, Y1, X2, Y2) = (%3.2f, %3.2f, %3.2f, %3.2f)" % (x1, y1, x2, y2))

        if Crop:
            if ROI == 'r':
                RS = RectangleSelector(current_ax, line_select_callback, drawtype='box', useblit=True, button=[1, 3],
                                       minspanx=5, minspany=5, spancoords='pixels', interactive=True)
            else:
                RS = EllipseSelector(current_ax, line_select_callback, drawtype='box', useblit=True, button=[1, 3],
                                     minspanx=5, minspany=5, spancoords='pixels', interactive=True)
        if Scale:
            RS = RectangleSelector(current_ax, line_select_callback, drawtype='line', useblit=True, button=[1, 3],
                                   minspanx=None, minspany=None, spancoords='pixels', interactive=True)
        plt.show()

        return RS.extents

    if Enable:
        plt.show()


def plot_2D_cuts(plt_cntnr, file_name=None, Enable=True, Silent=False, isphase=False, m=6.4, n=4.8, dpi=500,
                 x=None, y=None):
    """

    :param plt_cntnr:
    :param file_name:
    :param Enable:
    :param Silent:
    :param isphase:
    :param m:
    :param n:
    :param dpi:
    :param x:
    :param y:
    :return:
    """

    def get_slice(_image, _x, _y, _coords_x, _coords_y):
        """

        :param image:
        :param x:
        :param y:
        :param coords_x:
        :param coords_y:
        :return:
        """
        image_for_fit = np.nan_to_num(_image, True)

        f = interp2d(_x, _y, image_for_fit, kind='linear')
        if _coords_x == ':':
            # cut = f(_x, _coords_y)
            cut = image_for_fit[int(image_for_fit.shape[0] / 2), :]
            axis = _x
        if _coords_y == ':':
            # cut = f(_coords_x, _y)
            cut = image_for_fit[:, int(image_for_fit.shape[1] / 2)]
            axis = _y
        return np.asarray(cut), axis

    if x is None:
        x = (plt_cntnr.x[-1] + plt_cntnr.x[1]) / 2
    if y is None:
        y = (plt_cntnr.y[-1] + plt_cntnr.y[1]) / 2

    edges = [0, 0, 0, 0]

    if plt_cntnr.ax_limits[1] is None:
        if plt_cntnr.ax_limits[0] is None:
            edges[0] = plt_cntnr.x[0]
            edges[1] = plt_cntnr.x[-1]
        else:
            edges[0] = plt_cntnr.ax_limits[0]
    else:
        if plt_cntnr.ax_limits[0] is None:
            edges[1] = plt_cntnr.ax_limits[1]
        else:
            edges[0] = plt_cntnr.ax_limits[0]
            edges[1] = plt_cntnr.ax_limits[1]

    if plt_cntnr.ax_limits[3] is None:
        if plt_cntnr.ax_limits[2] is None:
            edges[2] = plt_cntnr.y[0]
            edges[3] = plt_cntnr.y[-1]
        else:
            edges[2] = plt_cntnr.ax_limits[2]
    else:
        if plt_cntnr.ax_limits[2] is None:
            edges[3] = plt_cntnr.ax_limits[3]
        else:
            edges[2] = plt_cntnr.ax_limits[2]
            edges[3] = plt_cntnr.ax_limits[3]

    if plt_cntnr.AspectRatio is True:
        dx = edges[1] - edges[0]
        dy = edges[3] - edges[2]
    else:
        dx = m
        dy = n

    left, bottom = 0.2, 0.10
    spacing = 0.02
    spacing_x = spacing
    spacing_y = spacing
    k = 0.25
    kx = k
    ky = k

    if dx >= dy:
        width = 0.50
        height = width * dy / dx
        spacing_y = spacing * dy / dx
        ky = k * dy / dx
    else:
        height = 0.50
        width = height * dx / dy
        spacing_x = spacing * dx / dy
        kx = k * dx / dy

    rect_image = [left, bottom, width, height]
    # rect_histx = [left, bottom + height + spacing_x + 0.02, width, kx]
    # rect_histy = [left + width + spacing_y + 0.02, bottom, ky, height]
    rect_histx = [left, bottom + height + spacing_x + 0.02, width, kx * .9]
    rect_histy = [left + width + spacing_x + 0.02, bottom, kx * .9, height]

    if plt_cntnr.AspectRatio is True:
        m = 6.4
        n = 6.4

    plt_settings(plt_cntnr.FontsSize, plt_cntnr.FontsSizeScale, plt_cntnr.LaTex, _hold=False, m=m, n=n)

    ax_image = plt.axes(rect_image)
    ax_image.tick_params(top=False, right=False)

    plt.xlabel(plt_cntnr.legends[1])
    plt.ylabel(plt_cntnr.legends[2])

    ax_histx = plt.axes(rect_histx, sharex=ax_image)
    # ax_histx.tick_params(direction='in', which='both', labelbottom=False, top=True, right=True, labelsize=labelsize, colors='black')
    ax_histx.tick_params(direction='in', which='both', labelbottom=False, top=True, right=True, colors='black')
    ax_histx.spines['bottom'].set_color('black')
    ax_histx.spines['top'].set_color('black')
    ax_histx.spines['right'].set_color('black')
    ax_histx.spines['left'].set_color('black')

    if plt_cntnr.legends[0] is not None:
        plt.title(plt_cntnr.legends[0])

    ax_histy = plt.axes(rect_histy, sharey=ax_image)
    # ax_histy.tick_params(direction='in', which='both', labelleft=False, top=True, right=True, labelsize=labelsize, colors='black')
    ax_histy.tick_params(direction='in', which='both', labelleft=False, top=True, right=True, colors='black')
    ax_histy.spines['bottom'].set_color('black')
    ax_histy.spines['top'].set_color('black')
    ax_histy.spines['right'].set_color('black')
    ax_histy.spines['left'].set_color('black')

    if plt_cntnr.Scale == 0:
        ax_image.imshow(plt_cntnr.image, cmap=ESRF_colors_2D(plt_cntnr.ColorScheme),
                        extent=[plt_cntnr.x[0], plt_cntnr.x[-1], plt_cntnr.y[0], plt_cntnr.y[-1]], origin='lower',
                        vmax=plt_cntnr.plt_limits[1], vmin=plt_cntnr.plt_limits[0])

    if plt_cntnr.Scale == 1:
        ax_image.imshow(plt_cntnr.image, cmap=ESRF_colors_2D(plt_cntnr.ColorScheme),
                        extent=[plt_cntnr.x[0], plt_cntnr.x[-1], plt_cntnr.y[0], plt_cntnr.y[-1]], origin='lower',
                        norm=LogNorm(vmax=plt_cntnr.plt_limits[1], vmin=plt_cntnr.plt_limits[0]))

    ax_image.set_xlim((edges[0], edges[1]))
    ax_image.set_ylim((edges[2], edges[3]))

    if plt_cntnr.AspectRatio:
        ax_image._axes._axes.set_aspect('equal')
    else:
        ax_image._axes._axes.set_aspect('auto')

    ax_image.locator_params(tight=True, nbins=3)

    # vertical cut
    cut_x, axis_x = get_slice(plt_cntnr.image, plt_cntnr.x, plt_cntnr.y, _coords_x=':', _coords_y=y)

    if plt_cntnr.Scale == 0:
        ax_histx.plot(plt_cntnr.x, cut_x, color=ESRF_colors(0))
        # if plt_cntnr.FillBetween:
        #     ax_histx.fill_between(plt_cntnr.x, 0, cut_x, color=ESRF_colors(0), alpha=plt_cntnr.alpha)
    else:
        ax_histx.semilogy(plt_cntnr.x, cut_x, color=ESRF_colors(0))
        # if plt_cntnr.FillBetween:
        #     ax_histx.fill_between(plt_cntnr.x, 0, cut_x, color=ESRF_colors(0), alpha=plt_cntnr.alpha)

    ax_histx.set_xlim((edges[0], edges[1]))
    ax_histx.set_ylim((plt_cntnr.plt_limits[0], plt_cntnr.plt_limits[1]))
    if plt_cntnr.Scale == 0:
        ax_histx.locator_params(tight=True, nbins=3)

    if plt_cntnr.grid:
        ax_histx.grid(which='major', linestyle='--', linewidth=0.5, color='dimgrey')
        ax_histx.grid(which='minor', linestyle='--', linewidth=0.5, color='lightgrey')

    # horizontal cut
    cut_y, axis_y = get_slice(plt_cntnr.image, plt_cntnr.x, plt_cntnr.y, _coords_x=x, _coords_y=':')

    if plt_cntnr.Scale == 0:
        ax_histy.plot(cut_y, plt_cntnr.y, color=ESRF_colors(0))
        # if plt_cntnr.FillBetween:
        #     ax_histy.fill_between( cut_y[:,0], plt_cntnr.y, 0, step='mid',color=ESRF_colors(0), alpha=plt_cntnr.alpha)
    else:
        ax_histy.semilogx(cut_y, axis_y, color=ESRF_colors(0))
        # if plt_cntnr.FillBetween:
        #     ax_histy.fill_between(plt_cntnr.y, 0, cut_y, color=ESRF_colors(0), alpha=plt_cntnr.alpha)

    ax_histy.set_ylim((edges[2], edges[3]))
    ax_histy.set_xlim((plt_cntnr.plt_limits[0], plt_cntnr.plt_limits[1]))
    if plt_cntnr.Scale == 0:
        ax_histy.locator_params(tight=True, nbins=3)

    if plt_cntnr.grid:
        ax_histy.grid(which='major', linestyle='--', linewidth=0.5, color='dimgrey')
        ax_histy.grid(which='minor', linestyle='--', linewidth=0.5, color='lightgrey')

    if file_name is not None:
        plt.savefig(file_name, dpi=dpi, bbox_inches='tight')
        file_name = file_name.split('/')
        print('>>>> file %s saved to disk.' % file_name[-1])
        if Silent:
            plt.close()

    if Enable:
        plt.show()


# ****************************************************************************
# ********************** 3D plots
# ****************************************************************************


def plot_3D():
    print('Place holder: not implemented yet...')


# ****************************************************************************
# ********************** Bar plots
# ****************************************************************************


def plot_bar():
    print('Place holder: not implemented yet...')


def plot_wft_coeffs(plt_cntnr, file_name=None, Enable=True, Silent=False, Pol=0, Noll=False, m=10, n=3, dpi=500):
    """

    :param plt_cntnr:
    :param file_name:
    :param Enable:
    :param Silent:
    :param Pol:
    :param Noll:
    :param m:
    :param n:
    :param dpi:
    :return:
    """

    plt_settings(plt_cntnr.FontsSize, plt_cntnr.FontsSizeScale, plt_cntnr.LaTex, _hold=False, m=m, n=n)

    if plt_cntnr.legends[0] is not None:
        plt.title(plt_cntnr.legends[0])
    if plt_cntnr.legends[1] is not None:
        plt.xlabel(plt_cntnr.legends[1])
    if plt_cntnr.legends[2] is not None:
        plt.ylabel(plt_cntnr.legends[2])

    anti_symmetric = deepcopy(plt_cntnr.image)
    symmetric = deepcopy(plt_cntnr.image) * np.nan

    if Pol == 0:  # Circular Zernike Polynomials
        N = 37

        if Noll:
            Zern_x_ticks = ['Z$_{1}$',
                            '$\cdot$', '$\cdot$',
                            '$\cdot$', 'Z$_{5}$', '$\cdot$',
                            '$\cdot$', '$\cdot$', '$\cdot$', 'Z$_{10}$',
                            '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'Z$_{15}$',
                            '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'Z$_{20}$', '$\cdot$',
                            '$\cdot$', '$\cdot$', '$\cdot$', 'Z$_{25}$', '$\cdot$', '$\cdot$', '$\cdot$',
                            '$\cdot$', 'Z$_{30}$', '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'Z$_{35}$', '$\cdot$',
                            '$\cdot$'
                            ]
        else:
            Zern_x_ticks = ['Z$_{0}^{0}$',
                            'Z$_{1}^{1}$', 'Z$_{1}^{-1}$',
                            'Z$_{2}^{0}$', 'Z$_{2}^{-2}$', 'Z$_{2}^{2}$',
                            'Z$_{3}^{-1}$', 'Z$_{3}^{1}$', 'Z$_{3}^{-3}$', 'Z$_{3}^{3}$',
                            'Z$_{4}^{0}$', 'Z$_{4}^{2}$', 'Z$_{4}^{-2}$', 'Z$_{4}^{4}$', 'Z$_{4}^{-4}$',
                            'Z$_{5}^{1}$', 'Z$_{5}^{-1}$', 'Z$_{5}^{3}$', 'Z$_{5}^{-3}$', 'Z$_{5}^{5}$', 'Z$_{5}^{-5}$',
                            'Z$_{6}^{0}$', 'Z$_{6}^{-2}$', 'Z$_{6}^{2}$', 'Z$_{6}^{-4}$', 'Z$_{6}^{4}$', 'Z$_{6}^{-6}$',
                            'Z$_{6}^{6}$',
                            'Z$_{7}^{-1}$', 'Z$_{7}^{1}$', 'Z$_{7}^{-3}$', 'Z$_{7}^{3}$', 'Z$_{7}^{-5}$', 'Z$_{7}^{5}$',
                            'Z$_{7}^{-7}$', 'Z$_{7}^{7}$',
                            'Z$_{8}^{0}$',
                            ]

        symmetric[0] = anti_symmetric[0]  # bias
        symmetric[3] = anti_symmetric[3]  # defocus
        symmetric[10] = anti_symmetric[10]  # primary spherical
        symmetric[21] = anti_symmetric[21]  # secondary spherical
        symmetric[36] = anti_symmetric[36]  # tertiary spherical

    elif Pol == 1:  # Rectangular Zernike Polynomials
        N = 15
        Zern_x_ticks = ['Rz$_{1}$', '$\cdot$', '$\cdot$', '$\cdot$', 'Rz$_{5}$',
                        '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'Rz$_{10}$',
                        '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'Rz$_{15}$',
                        ]

    elif Pol == 2:  # Legendre Polynomials
        N = 44
        Zern_x_ticks = ['L$_{1}$', '$\cdot$', '$\cdot$', '$\cdot$', 'L$_{5}$',
                        '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'L$_{10}$',
                        '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'L$_{15}$',
                        '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'L$_{20}$',
                        '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'L$_{25}$',
                        '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'L$_{30}$',
                        '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'L$_{35}$',
                        '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$', 'L$_{40}$',
                        '$\cdot$', '$\cdot$', '$\cdot$', '$\cdot$',

                        ]
        symmetric[0] = anti_symmetric[0]  # piston
        symmetric[1] = anti_symmetric[1]  # x-tilt
        symmetric[2] = anti_symmetric[2]  # y-tilt
        symmetric[3] = anti_symmetric[3]  # x-defocus
        symmetric[5] = anti_symmetric[5]  # y-defocus
        symmetric[6] = anti_symmetric[6]  # primary x-coma
        symmetric[9] = anti_symmetric[9]  # primary y-coma
        symmetric[10] = anti_symmetric[10]  # primary x-spherical
        symmetric[14] = anti_symmetric[14]  # primary y-spherical
        symmetric[15] = anti_symmetric[15]  # secondary x-coma
        symmetric[20] = anti_symmetric[20]  # secondary y-coma
        symmetric[21] = anti_symmetric[21]  # secondary x-spherical
        symmetric[27] = anti_symmetric[27]  # secondary y-spherical
        symmetric[28] = anti_symmetric[28]  # tertiary x-coma
        symmetric[35] = anti_symmetric[35]  # tertiary y-coma
        symmetric[36] = anti_symmetric[36]  # tertiary x-spherical
        symmetric[43] = anti_symmetric[43]  # tertiary y-spherical

    ind = np.arange(N)  # the x locations for the groups

    # TODO: separate p1 in p1_a and p1_b to enable colocr code for polynomial pairs

    p1 = plt.bar(ind, anti_symmetric)
    p2 = plt.bar(ind, symmetric)

    # plt.xticks(ind, Zern_x_ticks, rotation=60, horizontalalignment= 'right')
    plt.xticks(ind, Zern_x_ticks, rotation=0, horizontalalignment='center')

    plt.setp(p1, color=ESRF_colors(2))
    plt.setp(p2, color=ESRF_colors(3))

    if plt_cntnr.label is not None:
        plt.legend((p1[0], p2[0]), ('Non-symmetric', 'Radially-symmetric'))

    plt.ylim((plt_cntnr.plt_limits[0], plt_cntnr.plt_limits[1]))
    plt.locator_params(tight=True)
    plt.locator_params(tight=True, nbins=plt_cntnr.nbins, axis='y')

    if plt_cntnr.grid:
        plt.grid(linestyle='--', linewidth=0.5, color='black')
    # else:
    plt.axhline(y=0, linewidth=0.5, color='black', alpha=0.75)

    if file_name is not None:
        plt.savefig(file_name, dpi=dpi, bbox_inches='tight')
        file_name = file_name.split('/')
        print('>>>> file %s saved to disk.' % file_name[-1])
        if Silent:
            plt.close()

    if Enable:
        plt.show()


# ****************************************************************************
# ********************** Other plots
# ****************************************************************************


def plot_quiver(plt_cntnr, fld_X, fld_Y, file_name=None, Enable=True, Silent=False, m=6.4, n=4.8, dpi=500, kk=50):
    plt_settings(plt_cntnr.FontsSize, plt_cntnr.FontsSizeScale, plt_cntnr.LaTex, _hold=False, m=m, n=n)

    if plt_cntnr.legends[0] is not None:
        plt.title(plt_cntnr.legends[0])
    if plt_cntnr.legends[1] is not None:
        plt.xlabel(plt_cntnr.legends[1])
    if plt_cntnr.legends[2] is not None:
        plt.ylabel(plt_cntnr.legends[2])

    plt.tick_params(direction='in', which='both')
    plt.tick_params(axis='x', pad=8)

    if plt_cntnr.ax_limits[1] is None:
        if plt_cntnr.ax_limits[0] is None:
            pass
        else:
            plt.xlim(xmin=plt_cntnr.ax_limits[0])
    else:
        if plt_cntnr.ax_limits[0] is None:
            plt.xlim(xmax=plt_cntnr.ax_limits[1])
        else:
            plt.xlim((plt_cntnr.ax_limits[0], plt_cntnr.ax_limits[1]))

    if plt_cntnr.ax_limits[3] is None:
        if plt_cntnr.ax_limits[2] is None:
            pass
        else:
            plt.ylim(ymin=plt_cntnr.ax_limits[2])
    else:
        if plt_cntnr.ax_limits[2] is None:
            plt.ylim(ymax=plt_cntnr.ax_limits[3])
        else:
            plt.ylim((plt_cntnr.ax_limits[2], plt_cntnr.ax_limits[3]))

    X, Y = np.meshgrid(plt_cntnr.x, plt_cntnr.y)
    kx = int((len(plt_cntnr.x) % kk) / 2)
    ky = int((len(plt_cntnr.y) % kk) / 2)

    if plt_cntnr.Colorbar:
        C = np.sqrt(fld_X ** 2 + fld_Y ** 2)
        im = plt.quiver(X[kx::kk, ky::kk], Y[kx::kk, ky::kk], fld_X[kx::kk, ky::kk], fld_Y[kx::kk, ky::kk],
                        C[kx::kk, ky::kk],
                        cmap=ESRF_colors_2D(plt_cntnr.ColorScheme), angles='xy', scale_units='xy')
        im.set_clim(vmin=plt_cntnr.plt_limits[1], vmax=plt_cntnr.plt_limits[0])
        if plt_cntnr.Scale == 1:
            plt.colorbar(format='%.0e')
        else:

            if plt_cntnr.AspectRatio:
                im_ratio = (plt_cntnr.y[-1] - plt_cntnr.y[0]) / (plt_cntnr.x[-1] - plt_cntnr.x[0])
            else:
                im_ratio = 1

            cb = plt.colorbar(im, fraction=0.046 * im_ratio, pad=0.04, extend=plt_cntnr.ColorbarExt,
                              spacing='uniform')

            if plt_cntnr.Scale != 2:
                def format_func(x, pos):
                    x = '%.2f' % x
                    pad = '' if x.startswith('-') else ' '
                    return '{}{}'.format(pad, x)

                tick_locator = ticker.MaxNLocator(nbins=4)
                cb.locator = tick_locator
                cb.update_ticks()
                if plt_cntnr.Scale != 2:
                    cb.ax.yaxis.major.formatter = ticker.FuncFormatter(format_func)
    else:
        im = plt.quiver(X[kx::kk, ky::kk], Y[kx::kk, ky::kk], fld_X[kx::kk, ky::kk], fld_Y[kx::kk, ky::kk],
                        cmap=ESRF_colors_2D(plt_cntnr.ColorScheme), angles='xy', scale_units='xy')

    im._axes.set(xlim=(plt_cntnr.x[0], plt_cntnr.x[-1]), ylim=(plt_cntnr.y[0], plt_cntnr.y[-1]))
    plt.locator_params(tight=True, nbins=plt_cntnr.nbins)

    if plt_cntnr.grid:
        plt.grid(linestyle='--', linewidth=0.4, color='dimgrey')

    if plt_cntnr.AspectRatio:
        im._axes._axes.set_aspect('equal')
    else:
        im._axes._axes.set_aspect('auto')

    if file_name is not None:
        plt.savefig(file_name, dpi=dpi, bbox_inches='tight')
        file_name = file_name.split('/')
        print('>>>> file %s saved to disk.' % file_name[-1])
        if Silent:
            plt.close()

    if Enable:
        plt.show()


def plot_contour(plt_cntnr, fld_X, fld_Y, file_name=None, Enable=True, Silent=False, m=6.4, n=4.8, dpi=500, kk=50):
    print('Place holder: not implemented yet...')
    # https://matplotlib.org/stable/plot_types/arrays/contour.html#sphx-glr-plot-types-arrays-contour-py


def plot_hist_1d(plt_cntnr, file_name=None, Enable=True, Silent=False, Hold=False, m=6.4, n=4.8, dpi=500,
                 nbins=None, wbins=None, rule='sqrt', norm=False, bold=True):
    plt_settings(plt_cntnr.FontsSize, plt_cntnr.FontsSizeScale, plt_cntnr.LaTex, _hold=Hold, m=m, n=n)

    if plt_cntnr.legends[0] is not None:
        plt.title(plt_cntnr.legends[0])
    if plt_cntnr.legends[1] is not None:
        plt.xlabel(plt_cntnr.legends[1])
    if plt_cntnr.legends[2] is not None:
        plt.ylabel(plt_cntnr.legends[2])

    plt.tick_params(direction='in', which='both')
    plt.tick_params(axis='x', pad=8)

    if plt_cntnr.ax_limits[1] is None:
        if plt_cntnr.ax_limits[0] is None:
            pass
        else:
            plt.xlim(xmin=plt_cntnr.ax_limits[0])
    else:
        if plt_cntnr.ax_limits[0] is None:
            plt.xlim(xmax=plt_cntnr.ax_limits[1])
        else:
            plt.xlim((plt_cntnr.ax_limits[0], plt_cntnr.ax_limits[1]))

    if nbins is None:
        # https://en.wikipedia.org/wiki/Histogram#Number_of_bins_and_width
        n = len(plt_cntnr.image.flatten())
        if wbins is not None:
            nbins = int((np.amax(plt_cntnr.image) - np.amin(plt_cntnr.image)) / wbins)
            if nbins < 2:
                nbins = 2
        elif rule == 'sqrt':
            nbins = int(np.sqrt(n))
        elif rule == 'sturge':
            nbins = int(np.log2(n)) + 1
        elif rule == 'rice':
            nbins = int(2 * n ** (1 / 3))
        elif rule == 'scotts':
            wbins = 3.49 * np.std(plt_cntnr.image.flatten()) * n ** (-1 / 3)
            nbins = int((np.amax(plt_cntnr.image) - np.amin(plt_cntnr.image)) / wbins)
        elif rule == 'freedman-diaconis':
            pass
        elif rule == 'doane':
            pass

    if plt_cntnr.label is not None:
        N, bins, patches = plt.hist(plt_cntnr.image.flatten(), bins=nbins, color=ESRF_colors(plt_cntnr.ColorScheme),
                                    alpha=plt_cntnr.alpha, label=plt_cntnr.label, density=norm, stacked=norm,
                                    histtype='bar')
        plt.legend(loc=plt_cntnr.LabelPos)

    else:
        N, bins, patches = plt.hist(plt_cntnr.image.flatten(), bins=nbins, color=ESRF_colors(plt_cntnr.ColorScheme),
                                    alpha=plt_cntnr.alpha, density=norm, stacked=norm, histtype='bar')

    if bold:
        plt.hist(plt_cntnr.image.flatten(), bins=bins, color=ESRF_colors(-1), alpha=plt_cntnr.alpha, density=norm,
                 stacked=norm, histtype='step')

    if plt_cntnr.ax_limits[3] is None:
        if plt_cntnr.ax_limits[2] is None:
            pass
        else:
            plt.ylim(ymin=plt_cntnr.ax_limits[2])
    else:
        if plt_cntnr.ax_limits[2] is None:
            plt.ylim(ymax=plt_cntnr.ax_limits[3])
        else:
            plt.ylim((plt_cntnr.ax_limits[2], plt_cntnr.ax_limits[3]))

    if plt_cntnr.grid:
        plt.grid(which='major', linestyle='--', linewidth=0.5, color='dimgrey')
        plt.grid(which='minor', linestyle='--', linewidth=0.5, color='lightgrey')

    if plt_cntnr.FillBetween:
        plt.fill_between(plt_cntnr.x, plt_cntnr.FillBetweenValue, plt_cntnr.image,
                         color=ESRF_colors(plt_cntnr.ColorScheme), alpha=plt_cntnr.alpha)

    if plt_cntnr.Scale != 0 or Hold is False:
        plt.locator_params(tight=True)  # , nbins=plt_cntnr.nbins)

    if plt_cntnr.xticks is not None:
        plt.xticks(plt_cntnr.xticksaxis, plt_cntnr.xticks, rotation=0, horizontalalignment='center')

    if file_name is not None:
        plt.savefig(file_name, dpi=dpi, bbox_inches='tight')
        file_name = file_name.split('/')
        print('>>>> file %s saved to disk.' % file_name[-1])
        if Silent:
            plt.close()

    if Enable:
        plt.show()

    return bins, N


if __name__ == '__main__':
    print('welcome to barc4plots')
