# barc4plots


**B**iblioteca **A**uxiliar do **R**afael **C**elestre for **P**lots.

This library allows make very pretty plots =D

Assuming that "barc4plots_home" is the absolute path to the full ```barc4plots``` directory after cloning it from GitLab.
You can install and run barc4plots by:

```commandline
$ cd barc4plots_home
$ pip install .
```