Table of contents:

- example_01.ipynb: 1D plots
-- simple 1D plot
-- multiple curves on the same figure
-- twin y-axis (place holder)

- example_02.ipynb: 2D plots
-- simple 2D plot
-- plotting wavefront phase

- example_03.ipynb: 3D plots

- example_04.ipynb: Zernike coefficients

- example_05.ipynb: dispersion plots (tutorial)

- example_06.ipynb: quiver plot (detector distortion)